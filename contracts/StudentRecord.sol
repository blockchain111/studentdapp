// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
pragma experimental ABIEncoderV2;

contract StudentRecord {
    uint256 public studentsCount = 0;
    // Model a Student
    struct Student {
        uint256 _id;
        uint256 sid;
        string name;
        string timestamp;
        bool graduated;
    }
    mapping(uint256 => Student) public students;

    constructor() public{}

    // Events
    event addStudentEvent(
        uint256 _id,
        uint256 indexed sid,
        string name,
        string timestamp,
        bool graduated
    );

    event markGraduatedEvent(uint256 indexed sid);

    //Change graduation status of student
    function markGraduated(uint256 _id) public returns (Student memory) {
        students[_id].graduated = true;
        // trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    //Fetch student info from storage
    function findStudent(uint256 _id) public view returns (Student memory) {
        return students[_id];
    }

    //Create and add student to storage
    function addStudent(uint256 _studentNumber, string memory _name,string memory _timestamp)
        public
        returns (Student memory)
    {
        studentsCount++;
        students[studentsCount] = Student(
            studentsCount,
            _studentNumber,
            _name,
            _timestamp,
            false
        );
        // trigger create event
        emit addStudentEvent(studentsCount, _studentNumber, _name,_timestamp, false);
        return students[studentsCount];
    }
}